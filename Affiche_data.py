import matplotlib.pyplot as plt
import json

from datetime import datetime
from math import log



Y_ACb=[74, 89, 99, 98, 110, 118, 113, 106, 102, 100, 94, 82, 86, 80, 86, 75, 87, 74, 71, 71, 73, 72, 68, 73, 78, 68, 66, 79, 73, 67, 62, 53, 62, 61, 65, 64, 55, 62, 48, 54, 47, 44, 43, 43, 42, 46, 52, 46, 35, 38, 40, 44, 34, 37, 34, 41, 42, 43, 39, 40, 35, 36, 33, 28, 30, 33, 33, 27, 33, 28, 22, 21, 28, 23, 27, 19, 30, 24, 24, 23, 23, 18, 21, 17, 18, 19, 21, 20, 16, 15, 19, 14, 11, 14, 16, 15, 10, 13, 9, 12, 10, 7, 10, 10, 9, 12, 12, 8, 6, 5, 5, 6, 8, 7, 6, 5, 7, 4, 5, 6, 9, 7, 6, 7, 5, 5, 5, 3, 3, 4, 4, 3, 3, 3, 3, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 2, 2, 2, 2, 2, 2, 2, 3, 3, 2, 1, 2, 2, 2, 2, 2, 3, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0]
Y_AC=[]
for i in range(150):
    Y_AC.append(Y_ACb[i]*350)

# Y142_l=[]
# for i in range(2,140):
#     Y142_l.append(log(Y142_b[i])/log(i))


with open("/home/kouril/Bureau/Cours/1A/Stage/compartiment.json",'r') as file:
    COMP = json.load(file)

with open("/home/kouril/Bureau/Cours/1A/Stage/autocell.json",'r') as file:
    txt="["+file.read()+"]"
    AUTOCELL = json.loads(txt)

with open("/home/kouril/Bureau/Cours/1A/Stage/data.json",'r') as file:
    DATA = json.load(file)


plt.close()

leg=[]

## Courbes brutes
pok=6
dataName="H_908604632"
Y=[DATA[dataName]["Y"]]#,list(map(lambda x:0.952387830010653*x**2 - 293.050897181487*x +32535.6637059425,range(150)))]
#list(map(lambda i:i**(-0.15*(1+i/150)),range(1,150)))]#+list(map(lambda x:x["Y"], AUTOCELL[-pok:]))
# Courbe basique rapprochant DATA["1A_142"] : list(map(lambda i:i**(-0.15*(1+i/150)),range(1,150)))
# Polynomiale list(map(lambda x:0,952387830010653x**2 – 293,050897181487x +32535,6637059425,range(150)))
leg += [DATA[dataName]["name"]]#,"0,952387830010653x**2 – 293,050897181487x\n+32535,6637059425"]#+list(map(lambda x:x["a"], AUTOCELL[-pok:]))
XY=[]
extremums=[]
for y in Y:
    extremums.append(max(y))

for x,y in XY:
    extremums.append(max(y))
if extremums!=[]:
    max_E=max(extremums)


for k in range(len(Y)):
    cor=1#max_E/extremums[k]
    print(cor)
    plt.plot([i for i in range(len(Y[k]))],list(map(lambda x:cor*x, Y[k]))) #normal one
    pik=2
    # plt.plot([i for i in range(pik,len(Y[k]))],list(map(lambda i:(log(cor*Y[k][i]/max_E))/log(i), range(pik,len(Y[k])))))

for x,y in XY:
    plt.plot(x,list(map(lambda x:max_E*x/extremums[k], Y[k])))

plt.legend(leg)

## Courbes journalières = 1 courbe par jour de la semaine
allY=[]
for Y in allY:
    YT=[[],[],[],[],[],[],[]]
    XT=[[],[],[],[],[],[],[]]
    for i in range(len(Y)):
        YT[i%7].append(Y[i])
        XT[i%7].append(i)
    for i in range(7):
        plt.plot(XT[i],YT[i])

##Point hebdomadiare
# allY=[]
# for Y in allY:
#     YT=[]
#     XT=[]
#     sum=0
#     c=0
#     for i in range(len(Y)):
#         sum+=Y[i]
#         c+=1
#         if i%7==6:
#             YT.append(sum/c)
#             XT.append(i)
#             sum=0
#             c=0
#     YT.append(sum/c)
#     XT.append(i)
#     plt.plot(XT,YT)

pok=4
Y=[]#DATA["1A_1421475872"]["Y"][:150]]+list(map(lambda x:x["Y"], AUTOCELL[-pok:]))
leg += []#DATA["1A_1421475872"]["name"]]+list(map(lambda x:f"pMax={x['']}; pT={x['pTransmission']}; pS={x['pSourceContamination']}", AUTOCELL[-pok:]))
XY=[]
extremums=[]
for y in Y:
    extremums.append(max(y))

for x,y in XY:
    extremums.append(max(y))
if extremums!=[]:
    max_E=max(extremums)


for k in range(len(Y)):
    YT=[]
    XT=[]
    sum=0
    c=0
    cor=max_E/extremums[k]
    print(cor)
    for i in range(len(Y[k])):
        sum+=Y[k][i]*cor
        c+=1
        if i%7==6:
            YT.append(sum/c)
            XT.append(i)
            sum=0
            c=0
    YT.append(sum/c)
    XT.append(i)
    plt.plot(XT,YT)

for x,y in XY:
    plt.plot(x,list(map(lambda x:max_E*x/extremums[k], Y[k])))

plt.legend(leg)

##
plt.legend(leg)

d=datetime.now()
if input("Is the name defined")=="Y":
    name=dataName+"_Data"
    path = "/home/kouril/Bureau/Cours/1A/Stage/Images/"
    nameF = f"{name}_{d.year}-{d.month}-{d.day}_{d.hour}-{d.minute}-{d.second}.png"
    plt.savefig(path+nameF)

plt.show()