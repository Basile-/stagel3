import sympy as sym
from sympy import Eq
from sympy.plotting import plot
from IPython.display import display

x = sym.Symbol('x')

z   = 0.99
p11 = 0.99
p12 = 1-p11
p22 = 0.9
p23 = 0.05
p24 = 0.04
p25 = 1-p22-p23-p24
p32 = 0.1
p33 = 0.8
p34 = 1-p32-p33
p53 = 0.01
p55 = 1-p53

S = sym.Symbol('S')
C = sym.Symbol('C')
G = sym.Symbol('G')
II = sym.Symbol('II')
F = sym.Symbol('F')

sol = sym.solve([
            Eq(z+p11*x*S,S),
            Eq((1-z)+x*p12*S+x*p22*C+x*p32*G,C),
            Eq(p33*x*G+x*p23*C+x*p53*F,G),
            Eq(x*II+x*p34*G+x*p24*C,II),
            Eq(x*p55*F+x*p25*C,F)]
            ,
            (S,C,G,II,F))


p=plot(sol[C], xlim=[0.8,1.5], title="En phase d'écoute", ylim=[-10,10], adaptive=False, nb_of_points=1000, line_color='blue')

"""
p1 = plot(sol[C], (x, 0, 500,1))
"""

Ss  = sol[S].series()
Cs  = sol[C].series()
Gs  = sol[G].series()
IIs = sol[II].series()
Fs  = sol[F].series()

Ses= sym.apart(sol[S])
Ces= sym.apart(sol[C])
Ges= sym.apart(sol[G])
IIes= sym.apart(sol[II])
Fes= sym.apart(sol[F])




##
import matplotlib.pyplot as plt
from math import ceil
from time import time

#redefinition de fonctions pour utiliser matplotlib
def s(z): return sol[S].subs(x,z).evalf()
def c(z): return sol[C].subs(x,z).evalf()
def g(z): return sol[G].subs(x,z).evalf()
def ii(z): return sol[II].subs(x,z).evalf()
def f(z): return sol[F].subs(x,z).evalf()

m=1.01 #borne min affichée
M=1.015 #borne max affichée
T=0.000005 #pas
X=[i*T for i in range(ceil(m/T),ceil(M/T))]
Y=[s(i*T) for i in range(ceil(m/T),ceil(M/T))]
##
m=0
M=500
T=1

def func_to_list_of_coef_SE(nmax, X):
    Xs=X.series(n=nmax)
    XL=[]
    for i in range(m,nmax):
        XL.append(Xs.subs(x,0).evalf())
        Xs-=XL[-1]
        Xs=Xs/x
        Xs=Xs.simplify()
        #print(str(Xs))
    return XL
t1=time()
Xs=Ces.series(n=M)
print(time()-t1)
##
t1=time()
M=500
XL=[]
for i in range(M):
    XL.append(Xs.subs(x,0).evalf())
    Xs-=XL[-1]
    Xs=Xs/x
    Xs=Xs.simplify()

print(time()-t1)

##
# SL=func_to_list_of_coef_SE(M,sol[S])
CL=func_to_list_of_coef_SE(M,sol[C])
# Ss  = sol[S].series(n=M)
# Cs  = sol[C].series(n=M)
# Gs  = sol[G].series(n=M)
# IIs = sol[II].series(n=M)
# Fs  = sol[F].series(n=M)

##
partfrac=sym.apart(sol[C],full=True).doit()

Y=[]
Exp=[[1],[1],[1]]

b1=1.00927404195565
a1=0.151965311187389/b1
b2=1.06890605042354
a2=-0.142656201035155/b2
b3=1.30949543668024
a3=-0.00930911015258218/b3

for i in range(M):
    Exp[0].append(Exp[0][i]*b1)
    Exp[1].append(Exp[1][i]*b2)
    Exp[2].append(Exp[2][i]*b3)
    Y.append(a1/Exp[0][-1] + a2/Exp[1][-1]  + a3/Exp[2][-1])
    # Y.append(Exp[0][-1] + Exp[1][-1]  + Exp[2][-1])
X=[i for i in range(M)]

plt.plot(X,Y)
plt.show()

