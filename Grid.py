import pandas
import json
import seaborn as sns
import matplotlib.pyplot as plt
from datetime import datetime, timedelta

plt.close()
with open("/home/kouril/Bureau/Cours/1A/Stage/data.json",'r') as file:
    dict_ = json.load(file)
data={"sng_id":[], "date":[], "nb_streams":[]}
for n in ["1A_1421475872", "1A_1485289502", "1A_1490385102"]: # ["4M_870090", "4M_983238", "4M_1048898", "4M_1132150", "4M_4108057", "4M_84764285"]:
    X=[]
    id=[]
    d=datetime.strptime(dict_[n]["firstDate"], "%Y-%m-%d")
    for x in dict_[n]["X"]:
        X.append(d+timedelta(x))
        id.append(dict_[n]["name"])
    data["sng_id"]+=id
    data["date"]+=X
    data["nb_streams"]+=dict_[n]["Y"]

dataFrame=pandas.DataFrame(data)
g = sns.FacetGrid(dataFrame, col="sng_id", col_wrap=3)
g.map(sns.lineplot, "date", "nb_streams")
g.figure.subplots_adjust(wspace=.03, hspace=.02)

d=datetime.now()
name="Datas_1A"
path = "/home/kouril/Bureau/Cours/1A/Stage/Images/"
nameF = f"{name}_{d.year}-{d.month}-{d.day}_{d.hour}-{d.minute}-{d.second}.png"
plt.savefig(path+nameF)
plt.show()
