## Modèle à 6 compartiments : Un état qui n'écoutera jamais
import re
import sympy as sym
import matplotlib.pyplot as plt

from sympy import Eq
from datetime import datetime


x = sym.Symbol('x')

z   = 0.94 #pourcentage initial sain
p11 = 0.45 #rester sain
p14 = 0.5 # ne jamais l'écouter
p12 = 1-p11-p14 #etre exposé
p22 = 0.96 #rester exposé
p23 = 0.01 #arreter l'écoute
p24 = 0.01 #ne plus jamais ecouter la chanson
p25 = 1-p22-p23-p24 #devenir fan
p32 = 0.1 #se remettre à écouter G->I
p33 = 0.8 #rester neutre G
p34 = 1-p32-p33 #devenir rebuté depuis guérit ??
p53 = 0.01 #être gueris F->G
p55 = 1-p53 #rester fan

probs = f"""z = {z}
p11 = {round(p11,3)}
p12 = {round(p12,3)}
p14 = {round(p14,3)}
p22 = {round(p22,3)}
p23 = {round(p23,3)}
p24 = {round(p24,3)}
p25 = {round(p25,3)}
p32 = {round(p32,3)}
p33 = {round(p33,3)}
p34 = {round(p34,3)}
p53 = {round(p53,3)}
p55 = {round(p55,3)}"""

S = sym.Symbol('S')
C = sym.Symbol('C')
G = sym.Symbol('G')
II = sym.Symbol('II')
F = sym.Symbol('F')

sol = sym.solve([
            Eq(z+p11*x*S,S),
            Eq((1-z)+x*p12*S+x*p22*C+x*p32*G,C),
            Eq(p33*x*G+x*p23*C+x*p53*F,G),
            Eq(x*II+x*p14*S+x*p34*G+x*p24*C,II),
            Eq(x*p55*F+x*p25*C,F)]
            ,
            (S,C,G,II,F))

Ses= sym.apart(sol[S])
Ces= sym.apart(sol[C])
Ges= sym.apart(sol[G])
IIes= sym.apart(sol[II])
Fes= sym.apart(sol[F])



s=""
partfracC=sym.apart(sol[C],full=True).doit()
print("C : " + str(partfracC))
s+=str(partfracC).replace(" ","")
partfracF=sym.apart(sol[F],full=True).doit()
print("F : " + str(partfracF))
s+=str(partfracF).replace(" ","")

rfloat = re.compile("[-+]?(?:\d*\.\d+|\d+)")
frac_deg1 = re.compile("[-+]?(?:\d*\.\d+|\d+)/\(x[-+]?(?:\d*\.\d+|\d+)\)")
frac_deg1bis = re.compile("[-+]?(?:\d*\.\d+|\d+)/\([-+]?(?:\d*\.\d+|\d+)\*x-1\.0\)")
frac_deg2 = re.compile("[-+]?(?:\d*\.\d+|\d+)/\(x[-+]?(?:\d*\.\d+|\d+)\)")

deg1 = frac_deg1.findall(s)
deg1bis = frac_deg1bis.findall(s)
deg2 = frac_deg2.findall(s)

AB = []
for i in range(len(deg1)):
    tmp = rfloat.findall(deg1[i])
    b = - float(tmp[1])
    a = - float(tmp[0])/b
    AB.append([a,b])
for i in range(len(deg1bis)):
    tmp = rfloat.findall(deg1bis[i])
    b = 1/float(tmp[1])
    a = - float(tmp[0])
    AB.append([a,b])

M=150
Y=[]
Exp=[1]*len(AB)

extremum=0

for i in range(M):
    sum=0
    for k in range(len(AB)):
        Exp[k]*=AB[k][1]
        sum+=AB[k][0]/Exp[k]
    Y.append(sum)
    if abs(sum)>=abs(extremum) : extremum = sum
X=[i for i in range(M)]
plt.close()
plt.plot(X,Y)
plt.text(M/2, extremum/3, ""+probs)
plt.title("Courbe d'écoute C+F")
#plt.show()

##
extremumR=0
PT=[1]*M
for i in range(M):
    if PT[i]>extremumR : extremumR = PT[i]

Rap_H=extremum/extremumR
PTN=[0]*M
for i in range(M) : PTN[i]=PT[i]*Rap_H
var=0
for i in range(M):
    var+=abs(Y[i]-PTN[i])
print(var)

##Save
#img
d=datetime.now()
path = "/home/kouril/Bureau/Cours/1A/Stage/Images/"
nameF = f"6_comp_J_{d.year}-{d.month}-{d.day}_{d.hour}-{d.minute}-{d.second}.png"
plt.savefig(path+nameF)

#json
with open("/home/kouril/Bureau/Cours/1A/Stage/compartiment.json",'r') as file:
    dict_ = json.load(file)


dict_[nameF]={
    'z' = z,
    'p11' = round(p11,3),
    'p12' = round(p12,3),
    'p14' = round(p14,3),
    'p22' = round(p22,3),
    'p23' = round(p23,3),
    'p24' = round(p24,3),
    'p25' = round(p25,3),
    'p32' = round(p32,3),
    'p33' = round(p33,3),
    'p34' = round(p34,3),
    'p53' = round(p53,3),
    'p55' = round(p55,3)
    }

with open("/home/kouril/Bureau/Cours/1A/Stage/compartiment.json",'w') as file:
    json.dump(dict_,file)