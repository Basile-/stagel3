from datetime import datetime, timedelta
import matplotlib.pyplot as plt
import json

path = "/home/kouril/Bureau/Cours/1A/Stage/Donnees_Deezer/"

nameF="H_1674114567"
songName="Fils de joie"
# (1A_1421475872, "Permission to Dance", "BTS)
# (1A_1485289502, "LOCO", "ITZY")
# (1A_1490385102, "Lalisa", "Lisa")
# # # (3M_7827696,"Ambiance à l'africaine", "Magic system")
# # # (3M_72787717, "Le Roy A Fait Battre Tambour","LucArbogast")
# # # (3M_72787723, "Le Grand Coureur",  "LucArbogast")
# # # (3M_77698477, "Tu es fou", "Magic system")
# # # (3M_687546992, "Elle est bonne sa mère", "Vegedream")
# # # (3M_775724372, "Dance Monkey", "Tones and I")
# # # (3M_783256962, "91's", "PNL")
# # # (3M_1231568482, "Hérisson", "Ricciu")
# # # (3M_1231568502, "Un homme, ça doit savoir tout faire", "Ricciu")      #PowerLaw
# # # (3M_1278411782, "I Don't Feel", "Ilan Abu Baker")                     #PowerLaw
# # # (3M_1278411812, "Maybe the People I See", "Ilan Abu Baker")           #PowerLaw
# # # (3M_1398038442, "Don't Leave Me", "BTS")
# # # (3M_1498546032, "My Universe", "Coldplay")
# # # (3M_1541428372, "Time Time", "Trei Degete")
# (4M_870090, "J'ai demande a la lune", "Indochine")
# (4M_983238, "J't'emmene au vent", "Louise Attaque")
# (4M_1048898, "3 nuits par semaine", "Indochine")
# (4M_1132150, "L'aventurier", "Indochine")
# (4M_4108057, "Les demons de minuit", "Images")
# (4M_84764285, "Les demons de minuit_small", "Images")
# # # (H_437046332, "Beggin'", "Måneskin")
# # # (H_641517922, "Les planètes", "M. Pokora")
# # # (H_908604632,"Save Your Tears", "The Weeknd")
# # # (H_1049999812, "Bande organisée", "Soso Maness")
# # # (H_1225307892, "Si on disait", "M. Pokora")
# # # (H_1437070342, "abcdefu", "GAYLE")
# # # (H_1573406562, "Démons", "Angèle")
# # # (H_1576169062, "Butter", "BTS")
# # # (H_1674114547, "Santé", "Stromae")                                    #Belle PowerLaw
# # # (H_1674114567, "Fils de joie", "Stromae")                             #Belle PowerLaw


#["1A_1421475872", "1A_1485289502", "1A_1490385102", "3M_7827696", "3M_72787717", "3M_72787723", "3M_77698477", "3M_687546992", "3M_775724372", "3M_783256962", "3M_1231568482", "3M_1231568502", "3M_1278411782", "3M_1278411812", "3M_1398038442", "3M_1498546032", "3M_1541428372", "4M_870090", "4M_983238", "4M_1048898", "4M_1132150", "4M_4108057", "4M_84764285", "H_437046332", "H_641517922", "H_908604632", "H_1049999812", "H_1225307892", "H_1437070342", "H_1573406562", "H_1576169062", "H_1674114547", "H_1674114567"]


##Clean
Date_Stream=[]
with open(path+nameF+".csv","r") as f:
    l=f.readline()
    l=f.readline()
    while l!="":
        L=l.split(",")
        Date_Stream.append([datetime.strptime(L[1],"%Y-%m-%d"),int(L[2])])
        l=f.readline()

Date_Stream.sort(key=lambda l:l[0])

firstDate=datetime.strftime(Date_Stream[0][0],"%Y-%m-%d")
dateTmp=Date_Stream[0][0]-timedelta(1)
valTmp=0
Cleaned_Date_Stream=[]
for d,v in Date_Stream:
    n=(d-dateTmp).days
    for i in range(n-1,-1,-1):
        Cleaned_Date_Stream.append([(d-timedelta(i)),int(v-i*(v-valTmp)/n)])
    dateTmp=d
    valTmp=v
X=[]
Y=[]
dateTmp=Date_Stream[0][0]
for d,v in Cleaned_Date_Stream:
    X.append(d)#(d-dateTmp).days)
    Y.append(v)

plt.close()
plt.plot(X,Y)
plt.show()

## Save
if input("Is the name defined")!="Y":
    raise "Name has to be defined"

with open("/home/kouril/Bureau/Cours/1A/Stage/data.json",'r') as file:
    dict_ = json.load(file)
if nameF in dict_:
    raise "Data already saved"
else:
    dict_[nameF]={"Y":Y, "name":songName, "firstDate":firstDate}


with open("/home/kouril/Bureau/Cours/1A/Stage/data.json",'w') as file:
    json.dump(dict_,file)
