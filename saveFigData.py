import matplotlib.pyplot as plt
import json

from datetime import datetime, timedelta
#, "", "", "", "H_1573406562", "H_1576169062", "H_1674114547", "H_1674114567"]
for dataName in ["H_908604632"]:

    plt.close()
    with open("/home/kouril/Bureau/Cours/1A/Stage/data.json",'r') as file:
        DATA = json.load(file)

    Y=DATA[dataName]["Y"]
    fD=datetime.strptime(DATA[dataName]["firstDate"],"%Y-%m-%d")
    X=[fD+timedelta(i) for i in range(len(Y))]

    plt.plot(X,Y)
    plt.legend([DATA[dataName]["name"]])

    d=datetime.now()
    name=dataName+"_Data"
    path = "/home/kouril/Bureau/Cours/1A/Stage/Images/"
    nameF = f"{name}_{d.year}-{d.month}-{d.day}_{d.hour}-{d.minute}-{d.second}.png"
    # plt.savefig(path+nameF)
    plt.show()

print("Done successfully !\n")