#include <bits/stdc++.h>
#include <iostream>
#include <fstream>
using namespace std;

#define nNode 100



int nTours = 150;
float pTransmission = 0.2;
float pMax=0.8;
float pSourceContamination = 0.05;
//float pContinueToListen = 10;
float TimeToLive = 10;
float pNeverListenAgain = 0.1;
float a = 0.85;
float densite=0.2;
char mode = 'M'; //Aleatoire ou Barabasi ou Matrix ou TextToGraph

int tr=0;
int so=0;
int ag=0;
int su=0;

struct Node{
    char state; //the one we will base on during the calculation
    char nextState; //the one we modify during the calculation
    vector <int> in; //vertex that could infect the Node
    int age;
};


struct Node graph[nNode];
int used[nNode];

inline double prob();
int randint(int min, int max);
int not_all_used();
int GenerateRandomGraph(float density);
int GenerateBarabasiGraph();
int GenerateMatrixGraph();

int countContaminated(int i);
int step();

int printGraph();
int saveGraph();
int textToGraph();

int saveFig(vector <int>);







int main() {
    if (mode=='A') GenerateRandomGraph(densite);
    else if (mode=='B') {
        for(int i=0; i<nNode; i++) {used[i]=0;}
        GenerateBarabasiGraph();
    }
    else if (mode=='T') textToGraph();
    else if (mode=='M') {
        if (not GenerateMatrixGraph()) return 0;
    }
    else {
        printf("Error : mode forbiden");
        return 0;
    }
    //saveGraph();
    printGraph();
    
    vector <int> Y;

    graph[0].state='C';
    for(int i=0; i<nTours; i++) Y.push_back(step());
    cout << '[';
    for(int i=0; i<nTours; i++) cout << Y[i] << ", ";
    cout << ']' << endl;

    int sumC=0, sumS=0, sumM=0, sumA=0;
    for(int i=0; i<nNode; i++){
        if (graph[i].state=='S') sumS++;
        else if (graph[i].state=='C') sumC++;
        else if (graph[i].state=='M') sumM++;
        else { printf("+A : %d\n",i); sumA++; }
    }
    printf("\nsumC=%d, sumS=%d, sumM=%d, sumA=%d\n", sumC, sumS, sumM, sumA);
    printf("Transmissions=%d, SourceContaminated=%d, Died from age=%d, Suffocated=%d\n", tr, so, ag, su);
    saveFig(Y);
    return 0;
}







inline double prob(void) {
  random_device rd; // used as random seed
  static mt19937 rng; // create random generator
  rng.seed(rd()); // input the seed into the rng
  uniform_real_distribution<double> probability(0, 1); // create distribution
  return probability(rng); // return the random probability
}

int randint(int min, int max){
    random_device rd;     // only used once to initialise (seed) engine
    mt19937 rng(rd());    // random-number engine used (Mersenne-Twister in this case)
    uniform_int_distribution<int> uni(min,max); // guaranteed unbiased
    return uni(rng);
}
int not_all_used(){
    for(int i=0; i<nNode; i++) {if (used[i]==0) return 1;}
    return 0;
}

int GenerateBarabasiGraph(){
    int k,pk,l,pl,nbE=0; //nb Edge
    while (not_all_used()){
        do { k=randint(0,nNode-1);
        }while(used[k]);
        do{ l=randint(0,nNode-1);
        }while(nbE && (k==l || pow(float(graph[l].in.size())/nbE, a)<prob())); //sort si nbE==0 OU k!=l ET P>Random
        graph[k].in.push_back(l);
        graph[l].in.push_back(k);
        nbE+=2;
        used[k]++;
        pk=k;
        pl=l;
        graph[k].state='S';
        graph[k].nextState='S';
    }
    return 1;
}

int GenerateRandomGraph(float density) {
    for(int i = 0; i < nNode; i++) {
        graph[i].state='S';
        graph[i].nextState='S';
        for(int j = 0; j < nNode; j++) {
            if(i != j && prob() < density) {
                graph[i].in.push_back(j);
            }
        }
    }
    return 0;
}
int GenerateMatrixGraph() {
    int m=10;
    if (m*m!=nNode){
        printf("Error : mode Matrix activated but nNode doesn't correspond to m\n");
        return 0;
    }
    for (int i=0; i<nNode;i++){
        graph[i].state='S';
        graph[i].nextState='S';
        graph[i].in.push_back((i+(m-1)*m)%(m*m));
        graph[i].in.push_back((i+m)%(m*m));
        graph[i].in.push_back((i-1+m)%m+(i/m)*m);
        graph[i].in.push_back((i+1)%m+(i/m)*m);
    }
    return 1;
}


int countContaminated(int i){
    int sum=0;
    for(int j=0; j<graph[i].in.size(); j++){
        if(graph[graph[i].in[j]].state=='C') {
            sum++;
            //printf("%d is contaminated\n",i);
        }

    }
    return sum;
}

int step(){
    for(int i=0; i<nNode; i++){
        if (graph[i].state=='S'){
            int inContaminated = countContaminated(i);
            if (prob()<pSourceContamination){
                so++;
                graph[i].nextState='C';
                graph[i].age++;
            }
            else if (inContaminated>graph[i].in.size()*pTransmission){
                tr++;
                graph[i].nextState='C';
                graph[i].age++;
            }

        }
        else if (graph[i].state=='C'){
            int inContaminated = countContaminated(i);
            if (graph[i].age>=TimeToLive){
                if (prob()<pNeverListenAgain) graph[i].nextState='M';
                else {
                    graph[i].nextState='S';
                    graph[i].age=0;
                }
                ag++;
            }
            else if (inContaminated>=graph[i].in.size()*pMax){ //suffocating
                if (prob()<pNeverListenAgain) graph[i].nextState='M';
                else {
                    graph[i].nextState='S';
                    graph[i].age=0;
                }
                su++;
            }
            /*else if (prob()<pNeverListenAgain){ //Will Never listen Again
                graph[i].nextState='M';
            }*/
            else {
                graph[i].nextState='C';
                graph[i].age++;
            }
        }
        else {
            //if (graph[i].state != 'M') printf("step Y %c\n",graph[i].state);
            graph[i].nextState=graph[i].state;
        }
    }
    int sum=0;
    for(int i=0; i<nNode; i++){
        graph[i].state = graph[i].nextState;
        if (graph[i].state == 'C') sum++;
    }
    return sum;
}









int printGraph(){
    int sum=0;
    for (int i=0; i<nNode; i++){
        cout << i << " -> [ ";
        for (int j=0; j<graph[i].in.size(); j++){
            cout << graph[i].in[j] << " ";
        }
        cout << ']' << endl;
        sum+=graph[i].in.size();
    }
    cout << endl << "|V| = " << sum << endl;
    return 1;
}
int saveGraph(){
    fstream my_file;
	my_file.open("out", ios::out);
	if (!my_file) cout << "File not created!"<<endl;
	else {
        my_file << nNode << endl;
        for (int i=0; i<nNode; i++){
            my_file << graph[i].in.size() << " ";
            for (int j=0; j<graph[i].in.size(); j++){
                my_file << graph[i].in[j] << " ";
            }
            my_file << endl;
        }
        cout << "Saving successfull!"<<endl;
	}
    return 1;
}

int textToGraph(){
    int i=0,j,n,m;
    cin>>m;
    if (m!=nNode) {
        throw invalid_argument("Error : nNode != m, I can't initialize the graph\n");
    }
    while(cin>>n){
        for(int k=0; k<n; k++){
            cin >> j;
            graph[i].in.push_back(j);
            graph[i].state='S';
            graph[i].nextState='S';
        }
        i++;
    }
    return 1;
}

int saveFig(vector <int> Y){
    fstream my_file;
	my_file.open("autocell.json", ios::app);
	if (!my_file) cout << "File not created!"<<endl;
	else {
        my_file << ",\n{\n\"pTransmission\" : "<<pTransmission<<",\n\"mode\" : \""<<mode<<"\",\n\"pSourceContamination\" : "<<pSourceContamination<<",\n\"TimeToLive\" : "<<TimeToLive<</*",\n\"pNeverListenAgain\" : "<<pNeverListenAgain<<*/",\n\"a\" : "<<a<<",\n\"densite\":"<<densite<<",\n\"Y\":[";
        for(int i=0;i<nTours-1;i++){my_file<<Y[i]<<", ";}
        my_file<<Y[nTours-1]<<"]\n}\n";
		my_file.close(); 
        cout << "Saving successfull!"<<endl;
	}
	return 0;
}