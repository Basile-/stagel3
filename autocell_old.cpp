#include <bits/stdc++.h>
#include <iostream>
#include <fstream>
using namespace std;

#define nNode 400


int nTours = 150;
float pTransmission = 0.3;
float pSourceContamination = 0.2;
float pContinueToListen = 0.7;
float pNeverListenAgain = 0.1;
float a = 0.85;
//int d=1;
float densite=0.2;
char mode = 'T'; //Aleatoire ou Barabasi ou TextToGraph


struct Node{
    char state; //the one we will base on during the calculation
    char nextState; //the one we modify during the calculation
    vector <int> in; //vertex that could infect the Node
};


struct Node graph[nNode];
int used[nNode];

inline double prob();
int randint(int min, int max);
int not_all_used();
int GenerateRandomGraphs(float density);
int GenerateBarabasiGraph();

int countContaminated(int i);
int step();

int printGraph();
int saveGraph();
int textToGraph();

int saveFig(vector <int>);







int main() {
    if (mode=='A') GenerateRandomGraphs(densite);
    else if (mode=='B') {
        for(int i=0; i<nNode; i++) {used[i]=0;}
        GenerateBarabasiGraph();
    }
    else if (mode=='T') textToGraph();
    else {
        printf("Error : mode forbiden");
        return 0;
    }
    //saveGraph();
    //printGraph();
    
    vector <int> Y;

    graph[0].state='C';
    for(int i=0; i<nTours; i++) Y.push_back(step());
    cout << '[';
    for(int i=0; i<nTours; i++) cout << Y[i] << ", ";
    cout << ']' << endl;

    int sumC=0, sumS=0, sumM=0, sumA=0;
    for(int i=0; i<nNode; i++){
        if (graph[i].state=='S') sumS++;
        else if (graph[i].state=='C') sumC++;
        else if (graph[i].state=='M') sumM++;
        else { printf("+A : %d\n",i); sumA++; }
    }
    printf("\nsumC=%d, sumS=%d, sumM=%d, sumA=%d\n", sumC, sumS, sumM, sumA);
    saveFig(Y);
    return 0;
}







inline double prob(void) {
  random_device rd; // used as random seed
  static mt19937 rng; // create random generator
  rng.seed(rd()); // input the seed into the rng
  uniform_real_distribution<double> probability(0, 1); // create distribution
  return probability(rng); // return the random probability
}

int randint(int min, int max){
    random_device rd;     // only used once to initialise (seed) engine
    mt19937 rng(rd());    // random-number engine used (Mersenne-Twister in this case)
    uniform_int_distribution<int> uni(min,max); // guaranteed unbiased
    return uni(rng);
}
int not_all_used(){
    for(int i=0; i<nNode; i++) {if (used[i]==0) return 1;}
    return 0;
}

int GenerateBarabasiGraph(){
    int k,pk,l,pl,nbE=0; //nb Edge
    while (not_all_used()){
        do { k=randint(0,nNode-1);
        }while(used[k]);
        do{ l=randint(0,nNode-1);
        }while(nbE && (k==l || pow(float(graph[l].in.size())/nbE, a)<prob())); //sort si nbE==0 OU k!=l ET P>Random
        graph[k].in.push_back(l);
        graph[l].in.push_back(k);
        nbE+=2;
        used[k]++;
        pk=k;
        pl=l;
        graph[k].state='S';
        graph[k].nextState='S';
    }
    return 1;
}

int GenerateRandomGraphs(float density) {
    for(int i = 0; i < nNode; i++) {
        graph[i].state='S';
        graph[i].nextState='S';
        for(int j = 0; j < nNode; j++) {
            if(i != j && prob() < density) {
                graph[i].in.push_back(j);
            }
        }
    }
    return 0;
}



int countContaminated(int i){
    int sum=0;
    for(int j=0; j<graph[i].in.size(); j++){
        if(graph[graph[i].in[j]].state=='C') {
            sum++;
            //printf("%d is contaminated\n",i);
        }

    }
    return sum;
}

int step(){
    int sum=0;
    for(int i=0; i<nNode; i++){
        if (graph[i].state=='S'){
            int inContaminated = countContaminated(i);
            sum+=inContaminated;
            if (prob()<pSourceContamination || (inContaminated>0 && inContaminated*prob()<pTransmission)){
                graph[i].nextState='C';
            }
        }
        else if (graph[i].state=='C'){
            if (prob()>pContinueToListen){ //stop listening
                graph[i].nextState='S';
            }
            else if (prob()<pNeverListenAgain){ //Will Never listen Again
                graph[i].nextState='M';
            }
            else graph[i].nextState='C';
        }
        else {
            //if (graph[i].state != 'M') printf("step Y %c\n",graph[i].state);
            graph[i].nextState=graph[i].state;
        }
    }
    sum=0;
    for(int i=0; i<nNode; i++){
        graph[i].state = graph[i].nextState;
        if (graph[i].state == 'C') sum++;
    }
    return sum;
}









int printGraph(){
    int sum=0;
    for (int i=0; i<nNode; i++){
        cout << i << " -> [ ";
        for (int j=0; j<graph[i].in.size(); j++){
            cout << graph[i].in[j] << " ";
        }
        cout << ']' << endl;
        sum+=graph[i].in.size();
    }
    cout << endl << "|V| = " << sum << endl;
    return 1;
}
int saveGraph(){
    fstream my_file;
	my_file.open("out", ios::app);
	if (!my_file) cout << "File not created!"<<endl;
	else {
        my_file << nNode << endl;
        for (int i=0; i<nNode; i++){
            my_file << graph[i].in.size() << " ";
            for (int j=0; j<graph[i].in.size(); j++){
                my_file << graph[i].in[j] << " ";
            }
            my_file << endl;
        }
        cout << "Saving successfull!"<<endl;
	}
    return 1;
}

int textToGraph(){
    int i=0,j,n,m;
    cin>>m;
    if (m!=nNode) {
        throw invalid_argument("Error : nNode != m, I can't initialize the graph\n");
    }
    while(cin>>n){
        for(int k=0; k<n; k++){
            cin >> j;
            graph[i].in.push_back(j);
            graph[i].state='S';
            graph[i].nextState='S';
        }
        i++;
    }
    return 1;
}

int saveFig(vector <int> Y){
    fstream my_file;
	my_file.open("autocell.json", ios::app);
	if (!my_file) cout << "File not created!"<<endl;
	else {
        my_file << ",\n{\n\"pTransmission\" : "<<pTransmission<<",\n\"mode\" : \""<<mode<<"\",\n\"pSourceContamination\" : "<<pSourceContamination<<",\n\"pContinueToListen\" : "<<pContinueToListen<<",\n\"pNeverListenAgain\" : "<<pNeverListenAgain<<",\n\"a\" : "<<a<<",\n\"densite\":"<<densite<<",\n\"Y\":[";
        for(int i=0;i<nTours-1;i++){my_file<<Y[i]<<", ";}
        my_file<<Y[nTours-1]<<"]\n}";
		my_file.close(); 
        cout << "Saving successfull!"<<endl;
	}
	return 0;
}