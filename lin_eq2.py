## Modèle à 5 compartiments

import re
import sympy as sym
import matplotlib.pyplot as plt

from sympy import Eq

x = sym.Symbol('x')

z   = 0.99 #pourcentage initial sain
p11 = 0.95 #rester sain
p12 = 1-p11 #etre exposé
p22 = 0.9 #rester exposé
p23 = 0.05 #arreter l'écoute
p24 = 0.04 #ne plus jamais ecouter la chanson
p25 = 1-p22-p23-p24 #devenir fan
p32 = 0.1 #se remettre à écouter G->I
p33 = 0.8 #rester neutre G
p34 = 1-p32-p33 #devenir directement fan
p53 = 0.01 #être gueris F->G
p55 = 1-p53 #rester fan

S = sym.Symbol('S')
C = sym.Symbol('C')
G = sym.Symbol('G')
II = sym.Symbol('II')
F = sym.Symbol('F')

sol = sym.solve([
            Eq(z+p11*x*S,S),
            Eq((1-z)+x*p12*S+x*p22*C+x*p32*G,C),
            Eq(p33*x*G+x*p23*C+x*p53*F,G),
            Eq(x*II+x*p34*G+x*p24*C,II),
            Eq(x*p55*F+x*p25*C,F)]
            ,
            (S,C,G,II,F))

Ses= sym.apart(sol[S])
Ces= sym.apart(sol[C])
Ges= sym.apart(sol[G])
IIes= sym.apart(sol[II])
Fes= sym.apart(sol[F])


M=150


partfrac=sym.apart(sol[C],full=True).doit()
print(partfrac)
s=str(partfrac).replace(" ","")

rfloat = re.compile("[-+]?(?:\d*\.\d+|\d+)")
frac_deg1 = re.compile("[-+]?(?:\d*\.\d+|\d+)/\(x[-+]?(?:\d*\.\d+|\d+)\)")
frac_deg1bis = re.compile("[-+]?(?:\d*\.\d+|\d+)/\([-+]?(?:\d*\.\d+|\d+)\*x-1\.0\)")
frac_deg2 = re.compile("[-+]?(?:\d*\.\d+|\d+)/\(x[-+]?(?:\d*\.\d+|\d+)\)")

deg1 = frac_deg1.findall(s)
deg1bis = frac_deg1bis.findall(s)
deg2 = frac_deg2.findall(s)

AB = []
for i in range(len(deg1)):
    tmp = rfloat.findall(deg1[i])
    b = - float(tmp[1])
    a = - float(tmp[0])/b
    AB.append([a,b])
for i in range(len(deg1bis)):
    tmp = rfloat.findall(deg1bis[i])
    b = 1/float(tmp[1])
    a = - float(tmp[0])
    AB.append([a,b])


Y=[]
Exp=[1]*len(AB)

for i in range(M):
    sum=0
    for k in range(len(AB)):
        Exp[k]*=AB[k][1]
        sum+=AB[k][0]/Exp[k]
    Y.append(sum)
X=[i for i in range(M)]
##
plt.plot(X,Y)
plt.text(60, 0.1, f"""[Erreur] : résolue
z = {z}
p11 = {p11}
p12 = {p12}
p22 = {p22}
p23 = {p23}
p24 = {p24}
p25 = {p25}
p32 = {p32}
p33 = {p33}
p34 = {p34}
p53 = {p53}
p55 = {p55}""")
plt.show()

